// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic']);

app.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
});

app.controller('myCtrl', ['$scope', '$http', function($scope, $http) {

    $scope.addContact = function(contact) {
        
        console.log(contact);
        
        // $http.post('http://limitless-tundra-9947.herokuapp.com/contacts.json', contact)
        //   .then(function(response) {
        //     console.log('contact added');
        //   }, function(response) {
        //     console.log('error');
        //   });

        // $http.put('http://limitless-tundra-9947.herokuapp.com/contacts/26.json', contact)
        //   .then(function(response) {
        //     console.log('contact added');
        //   }, function(response) {
        //     console.log('error');
        //   });

        $http.delete('http://limitless-tundra-9947.herokuapp.com/contacts/26.json')
          .then(function(response) {
            console.log('contact added');
          }, function(response) {
            console.log('error');
          });

    };

}]);